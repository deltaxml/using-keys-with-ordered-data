# Using Keys with Ordered Data

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---

*When and how to add DeltaXML 'keys' to your input data in order to have more control over the matching process*

This document describes how to run the sample. For concept details see: [Using Keys with Ordered Data](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/using-keys-with-ordered-data)


If you have Ant installed, use the build script provided to run the sample in the download directory e.g. samples/OrderedComparison. Simply type the following command to run the pipeline and produce the output files unkeyed-result.xml and keyed-result.xml.  


```
ant run
```

If you don't have Ant installed, you can run the sample from a command line by issuing the following commands from the sample directory (ensuring that you use the correct directory separators for your operating system).  Replace x.y.z with the major.minor.patch version number of your release e.g. `deltaxml-10.0.0.jar`  


```
java -jar ../../deltaxml-x.y.z.jar compare ordered documentA.xml documentB.xml unkeyed-result.xml
java -jar ../../deltaxml-x.y.z.jar compare ordered documentA-keyed.xml documentB-keyed.xml keyed-result.xml
```

To clean up the sample directory, run the following command in Ant:  


```
ant clean
```



